<!-- Header -->
<?php include 'header.php' ?>

<!-- Navbar Top -->
<?php include 'navbar-top.php' ?>

<!-- Welcome Section -->
<section class="bg-lokersolo pb-4">
  <div class="container">
    <div class="welcome mb-5">
      <div class="row">
        <div class="col-lg-8 col-sm-9">
          <h4 class="d-block mb-3">✌️ Portal Loker Solo</h4>
          <h1 class="welcome-title">Cari lowongan kerja di Solo</h1>
          <a href="pasang-loker.php" class="btn btn-primary btn-inline-flex mt-3 hide-desktop btn-cta-mobile">Pasang Lowongan<i class="fas fa-arrow-right ml-3 mr-1"></i></a>
        </div>
      </div>
      <!-- Subscribe -->
      <!-- <div class="row mt-5">
        <div class="col-lg-6">
          <label class="text-bold">Berlangganan Loker Solo</label>
          <div class="input-group mb-2">
            <input type="text" class="form-control" placeholder="Tulis alamat email">
            <div class="input-group-append">
              <button class="btn btn-gray" type="button">Submit</button>
            </div>
          </div>
          <div class="text-sm text-muted">
            Dapatkan informasi seputar lowongan kerja Solo di kotak pesan Anda!
          </div>
        </div>
      </div> -->
    </div>
    <div class="d-flex align-items-start justify-content-between">
      <h4 class="mb-2 mr-4">Rekomendasi Lowongan ❤️</h4>
      <div class="d-flex">
        <button type="button" id="go-backward" class="btn btn-dark"><i class="fas fa-arrow-left"></i></button>
        <button type="button" id="go-forward" class="btn btn-dark ml-2"><i class="fas fa-arrow-right"></i></button>
      </div>
    </div>
  </div>
  <div class="glide glide__full-height glide__front">
    <div class="glide__track" data-glide-el="track" style="padding-left: 15px;">
      <ul class="glide__slides">
        <li class="glide__slide">
          <!-- Card Item -->
          <a href="lowongan-detail.php" class="card-item">
            <div class="card border-0 shadow">
              <div class="card-body card-body-flex">
                <!-- Looping loker-->
                <div class="testdemoAnimation">
                  <div class="card-loker-top mb-3">
                    <div class="d-block text-muted">Dibutuhkan</div>
                    <h4 class="card-title-featured">Backend Web Developer (Profesional)</h4>
                    <div class="d-flex">
                      <div class="badge badge-primary mr-1">Full Time</div>
                      <div class="badge badge-secondary">S1</div>
                    </div>
                  </div>
                  <div class="card-loker-top mb-3">
                    <div class="d-block text-muted">Dibutuhkan</div>
                    <h4 class="card-title-featured">Frontend + Fullstack Developer  (React, Angular, Vue)</h4>
                    <div class="d-flex">
                      <div class="badge badge-primary mr-1">Full Time</div>
                      <div class="badge badge-secondary">SMK/D3</div>
                    </div>
                  </div>
                </div>
                <!-- End looping loker -->
                <div class="card-loker-img"></div>
                <div class="card-loker-bottom mt-3">
                  <h6 class="m-0 text-truncate">PT. Nusantara Berkah Digital</h6>
                  <div class="text-muted">Kota Solo</div>
                </div>
              </div>
            </div>
          </a><!-- End Card -->
        </li>
        <li class="glide__slide">
          <!-- Card Item -->
          <a href="lowongan-detail.php" class="card-item">
            <div class="card border-0 shadow">
              <div class="card-body card-body-flex">
                <!--
                  Ini bagian yang harus di loop ziz, nah urang kurang paham kalo mau looping siga kitu...
                -->
                <div class="card-loker-top mb-3">
                  <div class="d-block text-muted">Dibutuhkan</div>
                  <h4 class="card-title-featured">Admin Media Sosial</h4>
                  <div class="d-flex">
                    <div class="badge badge-primary mr-1">Full Time</div>
                    <div class="badge badge-secondary">SMA/SMK</div>
                  </div>
                </div><!-- Sampai sini yang harus di looping si div-nya -->
                <div class="card-loker-img"></div>
                <div class="card-loker-bottom mt-3">
                  <h6 class="m-0 text-truncate">Elshine Hijab</h6>
                  <div class="text-muted">Kota Solo</div>
                </div>
              </div>
            </div>
          </a><!-- End Card -->
        </li>
        <li class="glide__slide">
          <!-- Card Item -->
          <a href="lowongan-detail.php" class="card-item">
            <div class="card border-0 shadow">
              <div class="card-body card-body-flex">
                <!-- Looping loker -->
                <div class="testdemoAnimation">
                  <div class="card-loker-top mb-3">
                    <div class="d-block text-muted">Dibutuhkan</div>
                    <h4 class="card-title-featured">SPG/SPB</h4>
                    <div class="d-flex">
                      <div class="badge badge-primary mr-1">Full Time</div>
                      <div class="badge badge-secondary">Umum</div>
                    </div>
                  </div>
                  <div class="card-loker-top mb-3">
                    <div class="d-block text-muted">Dibutuhkan</div>
                    <h4 class="card-title-featured">Marketing Manager</h4>
                    <div class="d-flex">
                      <div class="badge badge-primary mr-1">Full Time</div>
                      <div class="badge badge-secondary">Umum</div>
                    </div>
                  </div>
                </div>
                <!-- End looping loker -->
                <div class="card-loker-img"></div>
                <div class="card-loker-bottom mt-3">
                  <h6 class="m-0 text-truncate">Batik Mbak Siti</h6>
                  <div class="text-muted">Kota Solo</div>
                </div>
              </div>
            </div>
          </a><!-- End Card -->
        </li>
        <li class="glide__slide">
          <!-- Card Item -->
          <a href="lowongan-detail.php" class="card-item">
            <div class="card border-0 shadow">
              <div class="card-body card-body-flex">
                <!--
                  Ini bagian yang harus di loop ziz, nah urang kurang paham kalo mau looping siga kitu...
                -->
                <div class="card-loker-top mb-3">
                  <div class="d-block text-muted">Dibutuhkan</div>
                  <h4 class="card-title-featured">Technical Sasles Retail</h4>
                  <div class="d-flex">
                    <div class="badge badge-primary mr-1">Full Time</div>
                    <div class="badge badge-secondary">D3</div>
                  </div>
                </div><!-- Sampai sini yang harus di looping si div-nya -->
                <div class="card-loker-img"></div>
                <div class="card-loker-bottom mt-3">
                  <h6 class="m-0 text-truncate">PT. Guna Bangun Jaya</h6>
                  <div class="text-muted">Kota Solo</div>
                </div>
              </div>
            </div>
          </a><!-- End Card -->
        </li>
        <li class="glide__slide">
          <!-- Card Item -->
          <a href="lowongan-detail.php" class="card-item">
            <div class="card border-0 shadow">
              <div class="card-body card-body-flex">
                <!--
                  Ini bagian yang harus di loop ziz, nah urang kurang paham kalo mau looping siga kitu...
                -->
                <div class="card-loker-top mb-3">
                  <div class="d-block text-muted">Dibutuhkan</div>
                  <h4 class="card-title-featured">Backend Web Developer</h4>
                  <div class="d-flex">
                    <div class="badge badge-primary mr-1">Full Time</div>
                    <div class="badge badge-secondary">S1</div>
                  </div>
                </div><!-- Sampai sini yang harus di looping si div-nya -->
                <div class="card-loker-img"></div>
                <div class="card-loker-bottom mt-3">
                  <h6 class="m-0 text-truncate">PT. Nusantara Berkah Digital</h6>
                  <div class="text-muted">Kota Solo</div>
                </div>
              </div>
            </div>
          </a><!-- End Card -->
        </li>
        <li class="glide__slide">
          <!-- Card Item -->
          <a href="lowongan-detail.php" class="card-item">
            <div class="card border-0 shadow">
              <div class="card-body card-body-flex">
                <!--
                  Ini bagian yang harus di loop ziz, nah urang kurang paham kalo mau looping siga kitu...
                -->
                <div class="card-loker-top mb-3">
                  <div class="d-block text-muted">Dibutuhkan</div>
                  <h4 class="card-title-featured">Admin Media Sosial</h4>
                  <div class="d-flex">
                    <div class="badge badge-primary mr-1">Full Time</div>
                    <div class="badge badge-secondary">SMA/SMK</div>
                  </div>
                </div><!-- Sampai sini yang harus di looping si div-nya -->
                <div class="card-loker-img"></div>
                <div class="card-loker-bottom mt-3">
                  <h6 class="m-0 text-truncate">Elshine Hijab</h6>
                  <div class="text-muted">Kota Solo</div>
                </div>
              </div>
            </div>
          </a><!-- End Card -->
        </li>
      </ul>
    </div>
  </div>
</section>
<section class="d-block py-5 bg-dark">
  <div class="container text-white text-center">
    <h2 class="m-0">🥇🥈🥉<h2>
    <h3 class="mt-0 mb-3 text-medium">Pasang iklan lowongan agar terhubung dengan pelamar potensial.</h3>
    <a href="" class="btn btn-primary d-inline-block">Pasang Lowongan Sekarang</a>
  </div>
</section>
<!-- List Lowongan -->
<section class="d-block py-5 bg-light mt-0">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 list-lowongan mb-5">
        <h4 class="d-block mb-3">Lowongan Terbaru ⚡</h4>
        <!-- Card Loker -->
        <a href="lowongan-detail.php" class="card border-0 shadow-sm mb-3">
          <div class="card-body card-body-flex-h align-items">
            <div class="card-loker-img-h flex-1"></div>
            <div class="d-flex flex-column ml-md-3 w-100 div-truncate">
              <div class="card-loker-top card-loker-top-fixed mb-3">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="text-muted show-detail-hover">Dibutuhkan</div>
                  <div class="text-muted text-sm">HARI INI</div>
                </div>
                <h4 class="mb-1 text-truncate" title="Backend Senior Web Developer (React JS)">Backend Senior Web Developer (React JS)</h4>
                <div class="d-flex align-items-center">
                  <div class="badge badge-primary mr-1">Full Time</div>
                  <div class="badge badge-secondary mr-2">S1</div>
                  <div class="text-sm">IDR 4.000.000 - 10.000.000</div>
                </div>
              </div>
              <div class="card-loker-bottom">
                <h6 class="m-0 text-truncate">PT. Nusantara Berkah Digital</h6>
                <div class="text-muted">Kota Solo</div>
              </div>
            </div>
          </div>
        </a><!-- End Card -->
        <!-- Card Loker -->
        <a href="lowongan-detail.php" class="card border-0 shadow-sm mb-3">
          <div class="card-body card-body-flex-h align-items">
            <div class="card-loker-img-h flex-1"></div>
            <div class="d-flex flex-column ml-md-3 w-100 div-truncate">
              <div class="card-loker-top card-loker-top-fixed mb-3">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="text-muted show-detail-hover">Dibutuhkan</div>
                  <div class="text-muted text-sm">HARI INI</div>
                </div>
                <h4 class="mb-1 text-truncate" title="Android Developer">Android Developer</h4>
                <div class="d-flex align-items-center">
                  <div class="badge badge-primary mr-1">Full Time</div>
                  <div class="badge badge-secondary mr-2">D3</div>
                  <div class="text-sm">IDR 4.000.000 - 10.000.000</div>
                </div>
              </div>
              <div class="card-loker-bottom">
                <h6 class="m-0 text-truncate">PT. Nusantara Berkah Digital</h6>
                <div class="text-muted">Kota Solo</div>
              </div>
            </div>
          </div>
        </a><!-- End Card -->
        <!-- Card Loker -->
        <a href="lowongan-detail.php" class="card border-0 shadow-sm mb-3">
          <div class="card-body card-body-flex-h align-items">
            <div class="card-loker-img-h flex-1"></div>
            <div class="d-flex flex-column ml-md-3 w-100 div-truncate">
              <div class="card-loker-top card-loker-top-fixed mb-3">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="text-muted show-detail-hover">Dibutuhkan</div>
                  <div class="text-muted text-sm">HARI INI</div>
                </div>
                <h4 class="mb-1 text-truncate" title="Tukang Masak">Tukang Masak</h4>
                <div class="d-flex align-items-center">
                  <div class="badge badge-warning mr-1">Part Time</div>
                  <div class="badge badge-secondary mr-2">Umum</div>
                  <div class="text-sm">Gaji Kompetitif</div>
                </div>
              </div>
              <div class="card-loker-bottom">
                <h6 class="m-0 text-truncate">Sogul Jugak</h6>
                <div class="text-muted">Kota Solo</div>
              </div>
            </div>
          </div>
        </a><!-- End Card -->
        <!-- Card Loker -->
        <a href="lowongan-detail.php" class="card border-0 shadow-sm mb-3">
          <div class="card-body card-body-flex-h align-items">
            <div class="card-loker-img-h flex-1"></div>
            <div class="d-flex flex-column ml-md-3 w-100 div-truncate">
              <div class="card-loker-top card-loker-top-fixed mb-3">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="text-muted show-detail-hover">Dibutuhkan</div>
                  <div class="text-muted text-sm">HARI INI</div>
                </div>
                <h4 class="mb-1 text-truncate" title="Sales Marketing">Sales Marketing</h4>
                <div class="d-flex align-items-center">
                  <div class="badge badge-primary mr-1">Full Time</div>
                  <div class="badge badge-secondary mr-2">SMA/SMK</div>
                  <div class="text-sm">IDR 1.200.000 - 2.000.000</div>
                </div>
              </div>
              <div class="card-loker-bottom">
                <h6 class="m-0 text-truncate">Putra Anugrah Sejahtera</h6>
                <div class="text-muted">Kota Solo</div>
              </div>
            </div>
          </div>
        </a><!-- End Card -->
        <a href="lowongan-detail.php" class="card border-0 shadow-sm mb-3">
          <div class="card-body card-body-flex-h align-items">
            <div class="card-loker-img-h flex-1"></div>
            <div class="d-flex flex-column ml-md-3 w-100 div-truncate">
              <div class="card-loker-top card-loker-top-fixed mb-3">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="text-muted show-detail-hover">Dibutuhkan</div>
                  <div class="text-muted text-sm">HARI INI</div>
                </div>
                <h4 class="mb-1 text-truncate" title="Sales Marketing">Sales Marketing</h4>
                <div class="d-flex align-items-center">
                  <div class="badge badge-primary mr-1">Full Time</div>
                  <div class="badge badge-secondary mr-2">SMA/SMK</div>
                  <div class="text-sm">IDR 1.200.000 - 2.000.000</div>
                </div>
              </div>
              <div class="card-loker-bottom">
                <h6 class="m-0 text-truncate">Putra Anugrah Sejahtera</h6>
                <div class="text-muted">Kota Solo</div>
              </div>
            </div>
          </div>
        </a><!-- End Card -->
        <a href="lowongan-detail.php" class="card border-0 shadow-sm mb-3">
          <div class="card-body card-body-flex-h align-items">
            <div class="card-loker-img-h flex-1"></div>
            <div class="d-flex flex-column ml-md-3 w-100 div-truncate">
              <div class="card-loker-top card-loker-top-fixed mb-3">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="text-muted show-detail-hover">Dibutuhkan</div>
                  <div class="text-muted text-sm">HARI INI</div>
                </div>
                <h4 class="mb-1 text-truncate" title="Sales Marketing">Sales Marketing</h4>
                <div class="d-flex align-items-center">
                  <div class="badge badge-primary mr-1">Full Time</div>
                  <div class="badge badge-secondary mr-2">SMA/SMK</div>
                  <div class="text-sm">IDR 1.200.000 - 2.000.000</div>
                </div>
              </div>
              <div class="card-loker-bottom">
                <h6 class="m-0 text-truncate">Putra Anugrah Sejahtera</h6>
                <div class="text-muted">Kota Solo</div>
              </div>
            </div>
          </div>
        </a><!-- End Card -->
        <a href="lowongan-detail.php" class="card border-0 shadow-sm mb-3">
          <div class="card-body card-body-flex-h align-items">
            <div class="card-loker-img-h flex-1"></div>
            <div class="d-flex flex-column ml-md-3 w-100 div-truncate">
              <div class="card-loker-top card-loker-top-fixed mb-3">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="text-muted show-detail-hover">Dibutuhkan</div>
                  <div class="text-muted text-sm">HARI INI</div>
                </div>
                <h4 class="mb-1 text-truncate" title="Sales Marketing">Sales Marketing</h4>
                <div class="d-flex align-items-center">
                  <div class="badge badge-primary mr-1">Full Time</div>
                  <div class="badge badge-secondary mr-2">SMA/SMK</div>
                  <div class="text-sm">IDR 1.200.000 - 2.000.000</div>
                </div>
              </div>
              <div class="card-loker-bottom">
                <h6 class="m-0 text-truncate">Putra Anugrah Sejahtera</h6>
                <div class="text-muted">Kota Solo</div>
              </div>
            </div>
          </div>
        </a><!-- End Card -->
        <a href="lowongan-detail.php" class="card border-0 shadow-sm mb-3">
          <div class="card-body card-body-flex-h align-items">
            <div class="card-loker-img-h flex-1"></div>
            <div class="d-flex flex-column ml-md-3 w-100 div-truncate">
              <div class="card-loker-top card-loker-top-fixed mb-3">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="text-muted show-detail-hover">Dibutuhkan</div>
                  <div class="text-muted text-sm">HARI INI</div>
                </div>
                <h4 class="mb-1 text-truncate" title="Sales Marketing">Sales Marketing</h4>
                <div class="d-flex align-items-center">
                  <div class="badge badge-primary mr-1">Full Time</div>
                  <div class="badge badge-secondary mr-2">SMA/SMK</div>
                  <div class="text-sm">IDR 1.200.000 - 2.000.000</div>
                </div>
              </div>
              <div class="card-loker-bottom">
                <h6 class="m-0 text-truncate">Putra Anugrah Sejahtera</h6>
                <div class="text-muted">Kota Solo</div>
              </div>
            </div>
          </div>
        </a><!-- End Card -->
        <nav aria-label="Page navigation example">
          <ul class="pagination">
            <li class="page-item disabled"><a class="page-link" href="#"><i class="fas fa-arrow-left"></i></a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#"><i class="fas fa-arrow-right"></i></a></li>
          </ul>
        </nav>
      </div>
      <div class="col-lg-4">
        <h4 class="d-block mb-3">Pencarian</h4>
        <div class="card border-0 shadow-sm mb-3">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
              <h6 class="m-0">Profesi</h6>
              <button type="button" class="btn btn-sm btn-light px-3" data-toggle="modal" data-target="#modalProfesi"><i class="fas fa-sm fa-plus"></i></button>
            </div>
            <ul class="list-selected-filter mt-2">
              <li class="selected-item">
                <i class="far fa-sm fa-window-close"></i>Barista
              </li>
              <li class="selected-item">
                <i class="far fa-sm fa-window-close"></i>Desainer
              </li>
              <li class="selected-item">
                <i class="far fa-sm fa-window-close"></i>Manajer
              </li>
            </ul>
          </div>
        </div>
        <div class="card border-0 shadow-sm mb-3">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
              <h6 class="m-0">Pendidikan</h6>
              <button type="button" class="btn btn-sm btn-light px-3" data-toggle="modal" data-target="#modalPendidikan"><i class="fas fa-sm fa-plus"></i></button>
            </div>
            <ul class="list-selected-filter mt-2">
              <li class="selected-item">
                <i class="far fa-sm fa-window-close"></i>S1
              </li>
            </ul>
          </div>
        </div>
        <div class="card border-0 shadow-sm mb-3">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
              <h6 class="m-0">Status Kerja</h6>
              <button type="button" class="btn btn-sm btn-light px-3" data-toggle="modal" data-target="#modalStatusKerja"><i class="fas fa-sm fa-plus"></i></button>
            </div>
            <ul class="list-selected-filter mt-2">
              <li class="selected-item">
                <i class="far fa-sm fa-window-close"></i>Full Time
              </li>
              <li class="selected-item">
                <i class="far fa-sm fa-window-close"></i>Part Time
              </li>
              <li class="selected-item">
                <i class="far fa-sm fa-window-close"></i>Freelance
              </li>
            </ul>
          </div>
        </div>
        <div class="card border-0 shadow-sm mb-3">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
              <h6 class="m-0">Lokasi</h6>
              <button type="button" class="btn btn-sm btn-light px-3" data-toggle="modal" data-target="#modalLokasiKerja"><i class="fas fa-sm fa-plus"></i></button>
            </div>
            <div class="text-sm text-muted mt-2">Pilih lokasi kerja</div>
          </div>
        </div>
        <div class="glide-sidebanner">
          <div class="glide__track" data-glide-el="track">
            <div class="glide__slides">
              <div class="glide__slide">
                <img src="images/ads-1.png" class="w-100 d-block">
              </div>
              <div class="glide__slide">
                <img src="images/ads-2.png" class="w-100 d-block">
              </div>
              <div class="glide__slide">
                <img src="images/ads-3.png" class="w-100 d-block">
              </div>
            </div>
          </div>
          <div class="glide__bullets" data-glide-el="controls[nav]">
            <button class="glide__bullet" data-glide-dir="=0"></button>
            <button class="glide__bullet" data-glide-dir="=1"></button>
            <button class="glide__bullet" data-glide-dir="=2"></button>
          </div>
        </div>
      </div><!-- class="col-md-4" -->
    </div>
  </div>
</section>
<!-- Footer -->
<?php include 'footer.php' ?>

<!-- Modal Profesi -->
<div class="modal" id="modalProfesi" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content border-0">
            <div class="modal-body">
              <h5 class="m-0 mb-1">Pilih Profesi</h5>
              <ul class="list-selected-filter filtering mt-2">
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Barista
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Desainer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Manajer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Barista
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Desainer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Manajer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Barista
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Desainer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Manajer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Barista
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Desainer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Manajer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Barista
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Desainer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Manajer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Barista
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Desainer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Manajer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Barista
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Desainer
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Manajer
                </li>
              </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm mr-3">Simpan</button>
                <button type="button" class="btn btn-link mr-3" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Pendidikan -->
<div class="modal" id="modalPendidikan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-body">
              <h5 class="m-0 mb-1">Pilih Pendidikan</h5>
              <ul class="list-selected-filter filtering mt-2">
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>SMA/SMK
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>D3
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>S1
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>S2
                </li>
              </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm mr-3">Simpan</button>
                <button type="button" class="btn btn-link mr-3" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Status Kerja -->
<div class="modal" id="modalStatusKerja" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal" role="document">
        <div class="modal-content border-0">
            <div class="modal-body">
              <h5 class="m-0 mb-1">Pilih Status Kerja</h5>
              <ul class="list-selected-filter filtering filtering-full mt-2">
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Fulltime
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Partime
                </li>
                <li class="selected-item">
                  <i class="far fa-sm fa-check-square"></i>Freelance
                </li>
              </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm mr-3">Simpan</button>
                <button type="button" class="btn btn-link mr-3" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>