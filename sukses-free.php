<!-- Header -->
<?php include 'header.php' ?>

<section class="centered-all">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <h4 class="d-block mb-2">Loker Solo — Free</h4>
                <h1 class="welcome-title mb-3">Terkirim!</h1>
                <div class="row">
                    <div class="col-md-6 mb-2">
                        <div class="input-group input-group-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Order ID</span>
                            </div>
                            <input type="text" class="form-control font-weight-bold" value="LOKER1853" readonly>
                        </div>
                    </div>
                    <div class="col-md-6 mb-2">
                        <div class="input-group input-group-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Tgl. Order</span>
                            </div>
                            <input type="text" class="form-control font-weight-bold " value="30 Nov 2019" readonly>
                        </div>
                    </div>
                </div>
                <h4 class="thanks-content d-block mt-4 font-normal">Terimakasih <span class="font-weight-bolder">PT. Cipta Perdana</span> 🙏, materi lowongan sudah masuk dalam sistem kami. Mohon tunggu persetujuan dari tim kami mengenai iklan Anda yang akan ditampilkan di platform <span class="font-weight-bolder">Lokersolo</span>.</h4>
                <br>
                <a href="index.php" class="btn-inline-flex btn btn-primary mb-2">Kembali ke halaman utama</a>
                <br>
                <a href="#">Butuh bantuan? Hubungi kami disini</a>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<?php include 'footer.php' ?>