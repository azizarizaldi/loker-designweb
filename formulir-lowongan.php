<!-- Header -->
<?php include 'header.php' ?>

<!-- Navbar Top -->
<?php include 'navbar-top.php' ?>

<!-- Modal Lowongan -->
<div class="modal" id="modalLowongan-1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-body">
                <h5 class="m-0 mb-1">Backend Developer</h5>
                <div class="d-flex align-items-center mb-4">
                    <div class="badge badge-primary mr-1">Full Time</div>
                    <div class="badge badge-secondary mr-2">S1</div>
                    <div class="text-sm">IDR 4.000.000 - 10.000.000</div>
                </div>
                <div class="d-block mb-4">
                    <h6 class="mt-0 mb-1">Deskripsi Pekerjaan</h6>
                    <p class="text-sm m-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo vero fugiat magni iure labore esse nesciunt, est aliquam molestias voluptatibus voluptatem nostrum, impedit odio accusamus illum quia laborum eaque.</p>
                </div>
                <div class="d-block">
                    <h6 class="mt-0 mb-1">Syarat Pekerjaan</h6>
                    <p class="text-sm m-0">
                        - Pendidikan terakhir SMU sederajat<br>
                        - Umur maksimal 25 tahun<br>
                        - Berpenampilan menarik, percaya diri, suka melayani, dan komunikatif<br>
                        - Bersedia bekerja dengan shift dan hari libur<br>
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link mr-3" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<section class="d-block py-5 bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="d-block mb-4">
                    <h6 class="m-0 mb-2">Form Order</h6>
                    <h2 class="m-0">Paket Gold ✨</h2>
                </div>
                <div class="card border-0 shadow-sm mb-3">
                    <div class="card-header">
                        <h6 class="m-0">Lowongan Pekerjaan (1)</h6>
                    </div>
                    <div class="card-body">
                        <!-- 
                            Empty State.
                            Tampilkan ini ketika tidak ada lowongan yang dibuat.
                        -->
                        <!-- <div class="d-block py-5 px-3 rounded bg-light border mb-3 text-muted text-sm text-center">
                            Anda belum menambahkan lowongan, pasang lowongan sekarang juga. ✌️
                        </div> -->
                        <div class="d-block p-3 rounded bg-light border mb-3">
                            <h5 class="m-0 mb-1">Backend Developer</h5>
                            <div class="d-flex align-items-center">
                                <div class="badge badge-primary mr-1">Full Time</div>
                                <div class="badge badge-secondary mr-2">S1</div>
                                <div class="text-sm">IDR 4.000.000 - 10.000.000</div>
                            </div>
                            <div class="btn-group btn-group-sm mt-3" role="group">
                                <button type="button" class="btn btn-link mr-3" data-toggle="modal" data-target="#modalLowongan-1">Lihat Lowongan</button>
                                <button type="button" class="btn btn-link mr-3">Edit</button>
                                <button type="button" class="btn btn-link btn-link-muted">Hapus</button>
                            </div>
                        </div>
                        <a href="tambah-lowongan.php" class="btn btn-block btn-neutral"><i class="fas fa-sm fa-plus mr-2"></i>Tambah Lowongan</a>
                    </div>
                </div>
                <div class="card border-0 shadow-sm mb-3">
                    <div class="card-header">
                        <h6 class="m-0">Profil Perusahaan</h6>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-group">
                                <label for="nama_perusahaan" class="text-sm">Nama Perusahaan<span class="color-primary">*</span></label>
                                <input type="text" class="form-control text-capitalize" id="nama_perusahaan" placeholder="Masukan nama perusahaan" require>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi_perusahaan" class="text-sm">Deskripsi Perusahaan<span class="color-primary">*</span></label>
                                <textarea class="form-control" id="deskripsi_perusahaan" placeholder="Tuliskan deskripsi perusahaan" rows="4"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi_perusahaan" class="text-sm">Kota/Kabupaten<span class="color-primary">*</span></label>
                                <select class="form-control">
                                    <option>Pilih kota/kabupaten</option>
                                    <option>Bandung</option>
                                    <option>Jakarta</option>
                                    <option>Surabaya</option>
                                    <option>Solo</option>
                                </select>
                            </div>
                            <div class="form-group mb-0">
                                <div class="text-sm d-block mb-2">Logo Perusahaan</div>
                                <div class="custom-file mb-1">
                                    <input type="file" class="form-control">
                                </div>
                                <div class="text-muted text-sm">Ukuran maks. 500kb</div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card border-0 shadow-sm mb-3">
                    <div class="card-header">
                        <h6 class="m-0">Durasi</h6>
                    </div>
                    <div class="card-body">
                        <input class="form-control" data-toggle="datepicker" placeholder="Masukan tanggal">
                    </div>
                </div>
                <div class="card border-0 shadow-sm mb-3">
                    <div class="card-header">
                        <h6 class="m-0">Kontak Perusahaan</h6>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-group">
                                <label for="email_perusahaan" class="text-sm">Email<span class="color-primary">*</span></label>
                                <input type="email" class="form-control" id="email_perusahaan" placeholder="contoh@email.com" require>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="telepon_perusahaan" class="text-sm">Nomor Telepon<span class="color-primary">*</span></label>
                                    <input type="number" class="form-control" id="telepon_perusahaan" placeholder="0812-xxx-xxx-xx" require>
                                </div>
                                <div class="form-group col-md-6 pl-md-4">
                                    <label for="telepon_perusahaan" class="text-sm">Dihubungi via WhatsApp?</span></label>
                                    <div class="d-block mt-2">
                                        <div class="pretty p-default p-round">
                                            <input type="checkbox" />
                                            <div class="state p-success-o">
                                                <label>Ya</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alamat_perusahaan" class="text-sm">Alamat Perusahaan</label>
                                <textarea class="form-control" id="alamat_perusahaan" placeholder="Tuliskan almat perusahaan" rows="4"></textarea>
                            </div>
                        </form>
                    </div>
                </div>
                <a href="sukses.php" class="btn btn-block btn-primary">Kirim Lowongan</a>
                <div class="d-block text-center mt-3">
                    <a href="formulir-lowongan.php" class="btn btn-link btn-link-muted">Batal</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<?php include 'footer.php' ?>