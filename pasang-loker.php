<!-- Header -->
<?php include 'header.php' ?>

<!-- Navbar Top -->
<?php include 'navbar-top.php' ?>

<!-- Modal -->
<div class="modal" id="selectPlatform" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Pilih pemasangan</h4>
                <p>Pilih platform untuk memasangkan iklanmu, kami menyediakan WhatsApp & Website</p>
                <div class="row">
                    <div class="col-6">
                        <div class="card card-cursor card-select-platform shadow border-0 mr-n1">
                            <div class="card-body text-center">
                                <i class="fab fa-whatsapp"></i>
                                <span class="sub font-weight-bolder">
                                    Melalui Whatsapp
                                </span>
                            </div>
                        </div>
                    </div>  
                    <div class="col-6">
                        <div class="card card-cursor card-select-platform shadow border-0 ml-n1">
                            <div class="card-body text-center">
                                <i class="fas fa-desktop"></i>
                                <span class="sub font-weight-bolder">
                                    Melalui Website
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link mr-3" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<section class="d-block bg-lokersolo">
    <div class="container">
        <div class="welcome mb-0">
            <div class="row">
                <div class="col-lg-8">
                    <h1 class="welcome-title">Pasang lowongan dengan mudah.</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="d-block py-4 bg-dark">
  <div class="container text-white py-3">
    <h3 class="mt-0 mb-1 text-medium">Platform terbaik untuk mendapatkan talenta terbaik di Kota Solo.</h3>
    <h4 class="font-normal mb-0">Yuk lihat paketnya dibawah ini:</h4>
  </div>
</section>
<section class="d-block py-5 bg-light">
    <div class="container">
        <div class="d-block mb-4">
            <h4 class="d-block mb-4">👇 Paket Pasang Lowongan</h4>
            <div class="row">
                <div class="col-lg-4 d-flex mb-3">
                    <div class="card border-0 shadow w-100">
                        <div class="card-body">
                            <div class="d-block pb-3 mb-3 border-bottom">
                                <div class="d-flex justify-content-between">
                                    <h1 class="m-0">Gold</h1>
                                </div>
                                <h5 class="m-0 mb-2 text-medium">Efektif untuk 10 Hari</h5>
                                <h4 class="m-0 font-normal">Rp 200.000</h4>
                            </div>
                            <h5 class="font-normal"><span class="text-bold">5 kali publikasi</span> di semua jaringan LokerSolo.id</h5>
                            <!-- <div class="d-block mb-3">
                                <div class="badge badge-dark mr-1">Website</div><div class="badge badge-dark mr-1">Aplikasi</div><div class="badge badge-dark mr-1">Google Jobs & Bisnis</div><div class="badge badge-dark mr-1">Instagram</div><div class="badge badge-dark mr-1">Facebook</div><div class="badge badge-dark mr-1">Twitter</div><div class="badge badge-dark mr-1">LinkedIn</div>
                            </div> -->
                            <ul class="text-base text-sm mb-3">
                                <li>5x posting di Instagram @lokersolo</li>
                                <li>5x posting di Instastory</li>
                                <li>5x posting di Facebook Lokersolo</li>
                                <li>5x posting di Line @lokersolo</li>
                            </ul>
                            <div class="d-block p-3 bg-gold rounded">
                                <div class="d-flex align-items-center justify-content-between mb-2">
                                    <p class="m-0 text-bold">Lowongan Rekomendasi</p>
                                    <p class="m-0">❤️</p>
                                </div>
                                <p class="mb-0">Lebih banyak dilihat! Bagian rekomendasi terletak paling atas halaman website & aplikasi</p>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#selectPlatform">Pasang Lowongan</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 d-flex mb-3">
                    <div class="card border-0 shadow w-100">
                        <div class="card-body">
                            <div class="d-block pb-3 mb-3 border-bottom">
                                <div class="d-flex justify-content-between">
                                    <h1 class="m-0">Silver</h1>
                                </div>
                                <h5 class="m-0 mb-2 text-medium">Efektif untuk 5 Hari</h5>
                                <h4 class="m-0 font-normal">Rp 100.000</h4>
                            </div>
                            <h5 class="font-normal"><span class="text-bold">3 kali publikasi</span> di semua jaringan LokerSolo.id</h5>
                            <!-- <div class="d-block mb-3">
                                <div class="badge badge-dark mr-1">Website</div><div class="badge badge-dark mr-1">Aplikasi</div><div class="badge badge-dark mr-1">Google Jobs & Bisnis</div><div class="badge badge-dark mr-1">Instagram</div><div class="badge badge-dark mr-1">Facebook</div><div class="badge badge-dark mr-1">Twitter</div><div class="badge badge-dark mr-1">LinkedIn</div>
                            </div> -->
                            <ul class="text-base text-sm mb-3">
                                <li>3x posting di Instagram @lokersolo</li>
                                <li>3x posting di Instastory</li>
                                <li>3x posting di Facebook Lokersolo</li>
                                <li>3x posting di Line @lokersolo</li>
                            </ul>
                        </div>
                        <div class="card-footer">
                            <a href="formulir-lowongan.php" class="btn btn-primary btn-block">Pasang Lowongan</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 d-flex mb-3">
                    <div class="card border-0 shadow w-100">
                        <div class="card-body">
                            <div class="d-block pb-3 mb-3 border-bottom">
                                <div class="d-flex justify-content-between">
                                    <h1 class="m-0">Bronze</h1>
                                </div>
                                <h5 class="m-0 mb-2 text-medium">Efektif untuk 2 Hari</h5>
                                <h4 class="m-0 font-normal">Rp 50.000</h4>
                            </div>
                            <h5 class="font-normal"><span class="text-bold">1 kali publikasi</span> di semua jaringan LokerSolo.id</h5>
                            <!-- <div class="d-block mb-3">
                                <div class="badge badge-dark mr-1">Website</div><div class="badge badge-dark mr-1">Aplikasi</div><div class="badge badge-dark mr-1">Google Jobs & Bisnis</div><div class="badge badge-dark mr-1">Instagram</div><div class="badge badge-dark mr-1">Facebook</div><div class="badge badge-dark mr-1">Twitter</div><div class="badge badge-dark mr-1">LinkedIn</div>
                            </div> -->
                            <ul class="text-base text-sm mb-3">
                                <li>1x posting di Instagram @lokersolo</li>
                                <li>1x posting di Instastory</li>
                                <li>1x posting di Facebook Lokersolo</li>
                                <li>1x posting di Line @lokersolo</li>
                            </ul>
                        </div>
                        <div class="card-footer">
                            <a href="formulir-lowongan.php" class="btn btn-primary btn-block">Pasang Lowongan</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-block p-4 rounded bg-light-2">
            <h5 class="d-block mb-4">Keterangan</h5>
            <ul class="list-normal list-number">
                <li>Setiap lowongan akan dipublikasi ulang dalam rentang waktu 2 hari sekali sesuai dengan paket yang anda pilih ke semua jaringan LokerSolo.id.</li>
                <li>Ketika sudah mencapai batas publikasi maka iklan tidak akan dihapus, namun akan tergeser dengan iklan terbaru.</li>
                <li>Lowongan akan dipublikasikan urut sesuai dengan antrian order yang masuk dalam sistem LokerSolo.id.</li>
                <li>Anda dapat meminta kwitansi kepada kami dengan cara menghubungi kami via WA (apabila diperlukan).</li>
                <li>LokerSolo.id tidak bertanggungjawab atas segala materi lowongan dan akibatnya. Namun kami akan menghapus jika lowongan terbukti penipuan ataupun merugikan masyarakat.</li>
            </ul>
        </div>
    </div>
</section>
<section class="d-block py-5">
    <div class="container">
        <h4 class="d-block mb-4">Cara Pasang Lowongan</h4>
        <div class="d-block">
            <div class="row">
                <div class="col-lg-3 d-flex mb-3">
                    <div class="card border-0 shadow w-100">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h1 class="m-0">1</h1>
                                <img src="images/icon-1.png" width="36" height="36">
                            </div>
                            <p class="m-0">Pilih paket pemasangan lowongan sesuai yang anda inginkan</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex mb-3">
                    <div class="card border-0 shadow w-100">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h1 class="m-0">2</h1>
                                <img src="images/icon-2.png" width="36" height="36">
                            </div>
                           
                            <p class="m-0">Isi form lowongan dengan jelas sesuai daftar isian yang harus anda isi</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex mb-3">
                    <div class="card border-0 shadow w-100">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h1 class="m-0">3</h1>
                                <img src="images/icon-3.png" width="36" height="36">
                            </div>
                            <p class="m-0">Setelah pengisian form berhasil, anda akan diberikan intruksi pembayaran</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex mb-3">
                    <div class="card border-0 shadow w-100">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h1 class="m-0">4</h1>
                                <img src="images/icon-4.png" width="36" height="36">
                            </div>
                            <p class="m-0">Apabila pembayaran sudah dilakukan, lowongan akan dipublikasikan</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<?php include 'footer.php' ?>