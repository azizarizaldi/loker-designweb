<!-- Header -->
<?php include 'header.php' ?>

<!-- Navbar Top -->
<?php include 'navbar-top.php' ?>

<section class="d-block bg-lokersolo">
    <div class="container">
        <div class="welcome mb-0">
            <div class="row">
                <div class="col-lg-8">
                    <h1 class="welcome-title">Kontak</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="d-block py-5">
<div class="container">
    <div class="d-block">
        <ul class="text-base">
            <li>WhatsApp: +62 81313 664 669</li>
            <li>IG: @lokersolo</li>
            <li>Line: @lokersolo</li>
            <li>Email: cs@lokersolo.id</li>
        </ul>
    </div>
</div>
</section>

<!-- Footer -->
<?php include 'footer.php' ?>