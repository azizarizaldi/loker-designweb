<!-- Header -->
<?php include 'header.php' ?>

<!-- Navbar Top -->
<?php include 'navbar-top.php' ?>

<section class="d-block py-5 bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="d-flex align-items-center mb-3">
                    <a href="formulir-lowongan.php" class="btn btn-neutral"><i class="fas fa-sm fa-arrow-left mr-2"></i>Kembali</a>
                </div>
                <div class="card border-0 shadow-sm mb-3">
                    <div class="card-header">
                        <h6 class="m-0">Lowongan Pekerjaan</h6>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-group">
                                <label for="nama_pekerjaan" class="text-sm">Nama Pekerjaan<span class="color-primary">*</span></label>
                                <input type="text" class="form-control text-capitalize" id="nama_pekerjaan" placeholder="Masukan nama pekerjaan" require>
                            </div>
                            <div class="form-group">
                                <div class="d-flex align-items-center justify-content-between mb-2">
                                    <div class="text-sm">Kisaran Gaji</div>
                                    <div class="d-block">
                                        <div class="pretty p-default p-round text-sm mr-0">
                                            <input type="checkbox" name="gaji_kompetitif"/>
                                            <div class="state p-success-o">
                                                <label>Gaji Kompetitif</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group input-gaji">
                                    <input type="text" id="gaji_minimal" class="form-control" placeholder="Minimal gaji, misal 1 juta">
                                    <input type="text" id="gaji_maksimal" class="form-control" placeholder="Maksimal gaji, misal 3 juta">
                                </div>
                            </div>
                            <div class="form-row mb-3">
                                <div class="col-md-12">
                                    <label for="status_pekerjaan" class="text-sm">Status Pekerjaan<span class="color-primary">*</span></label>
                                </div>
                                <div class="col-md-4">
                                    <div class="pretty p-default p-round">
                                        <input type="radio" name="status"/>
                                        <div class="state p-success-o">
                                            <label>Full Time</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="pretty p-default p-round">
                                        <input type="radio" name="status"/>
                                        <div class="state p-success-o">
                                            <label>Part Time</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="pretty p-default p-round">
                                        <input type="radio" name="status"/>
                                        <div class="state p-success-o">
                                            <label>Freelance</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tingkat_pendidikan" class="text-sm">Tingkat Pendidikan<span class="color-primary">*</span></label>
                                <select class="form-control">
                                    <option>Pilih tingkat pendidikan</option>
                                    <option>SMA/SMK</option>
                                    <option>Diploma 1</option>
                                    <option>Diploma 2</option>
                                    <option>Diploma 3</option>
                                    <option>Sarjana 1</option>
                                    <option>Sarjana 2</option>
                                    <option>Sarjana 3</option>
                                    <option>Umum</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi_pekerjaan" class="text-sm">Deskripsi Pekerjaan<span class="color-primary">*</span></label>
                                <textarea class="form-control" id="deskripsi_pekerjaan" placeholder="Tuliskan deskripsi pekerjaan" rows="8"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="syarat_pekerjaan" class="text-sm">Syarat Pekerjaan<span class="color-primary">*</span></label>
                                <textarea class="form-control" id="syarat_pekerjaan" placeholder="Tuliskan syarat pekerjaan" rows="8"></textarea>
                            </div>
                            <button class="btn btn-block btn-primary"><i class="fas fa-sm fa-save mr-2"></i>Simpan Lowongan</button>
                            <div class="d-block text-center mt-3">
                            <a href="formulir-lowongan.php" class="btn btn-link btn-link-muted">Batal</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<?php include 'footer.php' ?>