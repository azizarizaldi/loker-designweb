    
    <footer class="d-block py-4">
      <div class="container text-center">
        <div class="text-sm">
          ©2020 Lokersolo <a href="#">Kebijakan Privasi</a> dan <a href="#">Syarat & Ketentuan</a>
        </div>
      </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <!-- Glide Script -->
    <script src="js/glide.min.js"></script>
    <script>
      var forward = document.querySelector('#go-forward')
      var backward = document.querySelector('#go-backward')

      var glide = new Glide('.glide', {
          type: 'carousel',
          perView: 4,
          gap: 18,
          peek: {
            before: 120,
            after: 150
          },
          breakpoints: {
            1199: {
              perView: 3,
              peek: {
                before: 80,
                after: 110
              }
            },
            991: {
              perView: 2,
              peek: {
                before: 80,
                after: 110
              }
            },
            767: {
              perView: 2,
              peek: {
                before: 40,
                after: 70
              },
              autoplay: 5000
            },
            600: {
              perView: 2,
              peek: {
                before: 20,
                after: 50
              },
              autoplay: 5000
            },
            500: {
              perView: 1,
              peek: {
                before: 50,
                after: 80
              },
              autoplay: 5000
            },
          },
          // autoplay: 2000
        })

        forward.addEventListener('click', function(){
          glide.go('>')
        })
        backward.addEventListener('click', function(){
          glide.go('<')
        })

        glide.mount()
      </script>
      <script>
        var glide2 = new Glide('.glide-sidebanner', {
          type: 'carousel',
          perView: 1,
          gap: 0,
          autoplay: 3000
        })
        glide2.mount()
      </script>

      <!-- DATEPICKER -->
      <script src="js/datepicker.min.js"></script>
      <script>
        $('[data-toggle="datepicker"]').datepicker({
          autoHide: true
        });
      </script>
      <!-- SLICK JS -->
      <script src="vendor/slick/slick.min.js"></script>
      <script>
      $(document).ready(function(){
        $('.testdemoAnimation').slick({
          arrows: false,
          autoplay: true,
          autoplaySpeed: 1600,
          speed: 0,
          fade: true
        });
      });
      </script>
  </body>
</html>