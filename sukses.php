<!-- Header -->
<?php include 'header.php' ?>

<!-- Navbar Top -->
<?php include 'navbar-top.php' ?>

<section class="py-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <h4 class="d-block mb-2">Loker Solo — Paket Gold</h4>
                <h1 class="welcome-title mb-3">Terkirim!</h1>
                <div class="row">
                    <div class="col-md-6 mb-2">
                        <div class="input-group input-group-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">No. Invoice</span>
                            </div>
                            <input type="text" class="form-control font-weight-bold" value="INV1853" readonly>
                        </div>
                    </div>
                    <div class="col-md-6 mb-2">
                        <div class="input-group input-group-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Tgl. Order</span>
                            </div>
                            <input type="text" class="form-control font-weight-bold " value="30 Nov 2019" readonly>
                        </div>
                    </div>
                </div>
                <h4 class="thanks-content d-block mt-4 font-normal">Terimakasih <span class="font-weight-bolder">PT. Cipta Perdana</span> 🙏, materi lowongan sudah masuk dalam sistem kami. Silahkan untuk melakukan pembayaran <span class="font-weight-bolder">beserta kode unik</span> dibawah ini:</h4>
                <div class="d-block mt-3 mb-4">
                    <h1 class="m-0">Rp 200.0<span class="text-primary">65</span></h1>
                    <div class="text-sm text-muted d-block mt-1">👉 Kode unik digunakan untuk cek pembayaran secara otomatis</div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8 mb-3">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card border-0 shadow mb-3">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="mr-3">
                                    Logo Bank
                                    </div>
                                    <div class="m-0">
                                        <h6 class="m-0">Bank BCA</h6>
                                        <div>
                                            an. Loker Solo<br>
                                            012456789
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card border-0 shadow mb-3">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="mr-3">
                                    Logo Bank
                                    </div>
                                    <div class="m-0">
                                        <h6 class="m-0">Bank Mandiri</h6>
                                        <div>
                                            an. Loker Solo<br>
                                            012456789
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="d-block p-4 rounded bg-light-2">
                    <h5 class="d-block mb-4">Catatan ⚡</h5>
                    <ul class="list-normal list-number">
                        <li>Harap transfer lengkap dengan kode uniknya, apabila tidak sesuai maka sistem tidak dapat melakukan pengecekan secara otomatis.</li>
                        <li>Lowongan akan diterbitkan mulai hari ini apabila pengiriman materi dan transfer dilakukan sebelum jam 11.00 WIB, waktu setelahnya maka lowongan akan diterbitkan mulai besok pagi (hari minggu/tanggal merah libur).</li>
                        <li>Apabila ada kendala dalam proses transfer seperti salah memasukkan nominal transfer, anda dapat menghubungi pusat bantuan kami melalui chat <a href="#">WA disini</a>.</li>
                    </ul>
                </div>
                <div class="text-sm text-muted d-block mt-4">*Informasi pembayaran ini juga dikirim ke email anda.</div>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<?php include 'footer.php' ?>