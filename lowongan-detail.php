<!-- Header -->
<?php include 'header.php' ?>

<!-- Navbar Top -->
<?php include 'navbar-top.php' ?>

<section class="d-block py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-5">
                <div class="d-flex align-items-start justify-content-between mb-3 lowongan-detail-header">
                    <div class="mr-4 lowongan-detail-header-1">
                        <h6 class="m-0 mb-2 text-medium">Membuka Lowongan</h6>
                        <h2 class="m-0 mb-3">PT Nusantara Activa Indonesia</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae temporibus esse ab nemo aspernatur culpa doloribus dolor corrupti earum sint nostrum.</p>
                    </div>
                    <div class="flex-shrink-0 lowongan-detail-header-2">
                        <img height="60" src="images/logo-nusantara.jpg"/>
                    </div>
                </div>
                <div class="d-block p-3 bg-light rounded lowongan-content">
                    <h5 class="d-block mb-3 mt-2 text-medium">👇 Membutuhkan:</h5>
                    <div class="card border-0 shadow-sm card-clear-mobile">
                        <div class="card-body">
                            <div class="d-block mb-3 pb-3 border-bottom">
                                <div class="badge badge-primary mb-2">Full Time</div>
                                <h4 class="m-0">Backend Senior Developer (React JS)</h4>
                            </div>
                            
                            <div class="d-block pb-3 mb-3 border-bottom">
                                <h5 class="m-0 mb-3 text-medium">Ringkasan</h5>
                                <ul class="list-col">
                                    <li><i class="fas fa-graduation-cap fa-sm mr-1"></i>Pendidikan<span>:</span></li>
                                    <li>SMA/SMK</li>
                                    <li><i class="fas fa-venus-mars fa-sm mr-1"></i>Gender<span>:</span></li>
                                    <li>Pria/Wanita</li>
                                    <li><i class="fas fa-running fa-sm mr-1"></i>Umur<span>:</span></li>
                                    <li>Maks. 35 Tahun</li>
                                    <li><i class="fas fa-money-bill-alt fa-sm mr-1"></i>Besaran Gaji<span>:</span></li>
                                    <li>5 Juta - 7 Juta</li>
                                    <li><i class="fas fa-calendar-alt fa-sm mr-1"></i>Batas Lamaran<span>:</span></li>
                                    <li>1 Januari 2020</li>
                                    <li><i class="fas fa-map-marker-alt fa-sm mr-1"></i>Lokasi Kerja<span>:</span></li>
                                    <li>Jl. Kaliurang Km 6,5 No C 28, Kentungan, (depan Batalyon TNI 403), Kota Solo</li>
                                </ul>
                            </div>

                            <div class="d-block pb-1 mb-3 border-bottom">
                                <h5 class="m-0 mb-3 text-medium">Deskripsi Pekerjaan</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae temporibus esse ab nemo aspernatur culpa doloribus dolor corrupti earum sint nostrum.</p>
                            </div>
                            <div class="d-block pb-3 mb-3 border-bottom">
                                <h5 class="m-0 mb-3 text-medium">Syarat Pekerjaan</h5>
                                <ul class="text-base">
                                    <li>Menguasai React JS</li>
                                    <li>Dapat bekerja dibawah tekanan</li>
                                    <li>Baik & Sopan Santun</li>
                                    <li>Memiliki kendaraan sendiri</li>
                                </ul>
                            </div>
                            <div class="d-block pb-3 mb-3 border-bottom">
                                <h5 class="m-0 mb-3 text-medium">Kirim Lamaran</h5>
                                <ul class="list-col">
                                    <li><i class="fas fa-envelope fa-sm mr-1"></i>Email<span>:</span></li>
                                    <li>nusantara@nusantara.id</li>
                                    <li><i class="fas fa-phone-alt fa-sm mr-1"></i>No. Telepon<span>:</span></li>
                                    <li>+62812345678</li>
                                    <li><i class="fas fa-map-marker-alt fa-sm mr-1"></i>Alamat<span>:</span></li>
                                    <li>Jl. Kaliurang Km 6,5 No C 28, Kentungan, (depan Batalyon TNI 403), Kota Solo</li>
                                </ul>
                            </div>
                            <div class="d-block">
                                <a href="#" class="btn btn-outline-primary btn-block shadow-sm mb-2 btn-lg"><i class="far fa-envelope mr-2"></i>Lamar Melalui Email</a>
                                <a href="#" class="btn btn-whatsapp btn-block shadow-sm btn-lg"><i class="fab fa-whatsapp mr-2"></i>Lamar Melalui WhatsApp</a>
                                <div class="text-sm text-muted d-block mt-2">
                                    Perhatikan materi lowongan dengan teliti dan waspada terhadap segala penipuan
                                </div>
                                <a href="" class="text-muted text-sm link"><i class="fas fa-exclamation-circle mr-2"></i>Laporkan lowongan ini</a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-4">
                <div class="card border-0 shadow-sm mb-3">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-center">
                    <h6 class="m-0">Profesi</h6>
                    <div><i class="fas fa-sm fa-plus"></i></div>
                    </div>
                    <ul class="list-selected-filter mt-2">
                    <li class="selected-item">
                        <i class="far fa-sm fa-window-close"></i>Barista
                    </li>
                    <li class="selected-item">
                        <i class="far fa-sm fa-window-close"></i>Desainer
                    </li>
                    <li class="selected-item">
                        <i class="far fa-sm fa-window-close"></i>Manajer
                    </li>
                    </ul>
                </div>
                </div>
                <div class="card border-0 shadow-sm mb-3">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-center">
                    <h6 class="m-0">Pendidikan</h6>
                    <div><i class="fas fa-sm fa-plus"></i></div>
                    </div>
                    <ul class="list-selected-filter mt-2">
                    <li class="selected-item">
                        <i class="far fa-sm fa-window-close"></i>S1
                    </li>
                    </ul>
                </div>
                </div>
                <div class="card border-0 shadow-sm mb-3">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-center">
                    <h6 class="m-0">Status Kerja</h6>
                    <div><i class="fas fa-sm fa-plus"></i></div>
                    </div>
                    <ul class="list-selected-filter mt-2">
                    <li class="selected-item">
                        <i class="far fa-sm fa-window-close"></i>Full Time
                    </li>
                    <li class="selected-item">
                        <i class="far fa-sm fa-window-close"></i>Part Time
                    </li>
                    <li class="selected-item">
                        <i class="far fa-sm fa-window-close"></i>Freelance
                    </li>
                    </ul>
                </div>
                </div>
                <div class="card border-0 shadow-sm mb-3">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-center">
                    <h6 class="m-0">Lokasi</h6>
                    <div><i class="fas fa-sm fa-plus"></i></div>
                    </div>
                    <div class="text-sm text-muted mt-2">Pilih lokasi kerja</div>
                </div>
                </div>
                <div class="glide-sidebanner">
                <div class="glide__track" data-glide-el="track">
                    <div class="glide__slides">
                    <div class="glide__slide">
                        <img src="images/ads-1.png" class="w-100 d-block">
                    </div>
                    <div class="glide__slide">
                        <img src="images/ads-2.png" class="w-100 d-block">
                    </div>
                    <div class="glide__slide">
                        <img src="images/ads-3.png" class="w-100 d-block">
                    </div>
                    </div>
                </div>
                <div class="glide__bullets" data-glide-el="controls[nav]">
                    <button class="glide__bullet" data-glide-dir="=0"></button>
                    <button class="glide__bullet" data-glide-dir="=1"></button>
                    <button class="glide__bullet" data-glide-dir="=2"></button>
                </div>
                </div>
            </div><!-- class="col-md-4" -->
        </div>
    </div>
</section>

<!-- Footer -->
<?php include 'footer.php' ?>