<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Styles CSS -->
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="styles/css/datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="styles/css/font-awesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="styles/css/styles.css">
    <!-- Title -->
    <title>Loker Solo | Lowongan Kerja di Solo 2019</title>
  </head>
  <body>